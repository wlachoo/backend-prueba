<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220512210950 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE viaje_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE viajero_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE viaje (id INT NOT NULL, codigo VARCHAR(255) NOT NULL, numero_plazas INT NOT NULL, destino VARCHAR(255) NOT NULL, lugar VARCHAR(255) NOT NULL, precio DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE viajero (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, cedula VARCHAR(255) NOT NULL, viaje INT NOT NULL, fecha VARCHAR(255) NOT NULL, telefono VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE viaje_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE viajero_id_seq CASCADE');
        $this->addSql('DROP TABLE viaje');
        $this->addSql('DROP TABLE viajero');
    }
}
