<?php

namespace App\Repository;

use App\Entity\Viajero;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
 
/**
 * @extends ServiceEntityRepository<Viajero>
 *
 * @method Viajero|null find($id, $lockMode = null, $lockVersion = null)
 * @method Viajero|null findOneBy(array $criteria, array $orderBy = null)
 * @method Viajero[]    findAll()
 * @method Viajero[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajeroRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry,EntityManagerInterface $manager)
    {
        parent::__construct($registry, Viajero::class);
        $this->manager = $manager;
    }

    public function saveViajero($cedula, $nombre, $fecha, $telefono, $viaje)
    {
        $newViajero= new Viajero();

        $newViajero
            ->setCedula($cedula)
            ->setNombre($nombre)
            ->setFecha($fecha)
            ->setTelefono($telefono)
            ->setViaje($viaje);

        $this->manager->persist($newViajero);
        $this->manager->flush();
    }

    public function updateViajero(Viajero $viajero): Viajero
    {
        $this->manager->persist($viajero);
        $this->manager->flush();

        return $viajero;
    }


    public function removeViajero(Viajero $viajero)
    {
        $this->manager->remove($viajero);
        $this->manager->flush();
    }

    /**public function add(Viajero $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Viajero $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }**/

//    /**
//     * @return Viajero[] Returns an array of Viajero objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Viajero
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
