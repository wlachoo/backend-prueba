<?php

namespace App\Repository;

use App\Entity\Viaje;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @extends ServiceEntityRepository<Viajes>
 *
 * @method Viajes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Viajes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Viajes[]    findAll()
 * @method Viajes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry,EntityManagerInterface $manager)
    {
        parent::__construct($registry, Viaje::class);
        $this->manager = $manager;
    } 

    public function saveViaje($codigo, $numero_plazas, $destino, $lugar, $precio)
    {
        $newViajes= new Viaje();

        $newViajes
            ->setCodigo($codigo)
            ->setNumeroPlazas($numero_plazas)
            ->setDestino($destino)
            ->setLugar($lugar)
            ->setPrecio($precio);

        $this->manager->persist($newViajes);
        $this->manager->flush();
    }

    public function remove(Viaje $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function updateViaje(Viaje $viaje): Viaje
    {
        $this->manager->persist($viaje);
        $this->manager->flush();

        return $viaje;
    }


    public function removeViaje(Viaje $viaje)
    {
        $this->manager->remove($viaje);
        $this->manager->flush();
    }

//    /**
//     * @return Viajes[] Returns an array of Viajes objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Viajes
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
