<?php

namespace App\Controller;
use App\Repository\ViajeroRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class ViajeroController 
{
    private $viajeroRepository;
 
    public function __construct(ViajeroRepository $viajeroRepository)
    {
        $this->viajeroRepository = $viajeroRepository;
    }

    /**
     * @Route("viajero", name="add_viajeros", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $cedula = $data['cedula'];
        $nombre = $data['nombre'];
        $fecha = $data['fecha'];
        $viaje = $data['viaje'];
        $telefono = $data['telefono'];

        if (empty($cedula) || empty($nombre) || empty($fecha) || empty($telefono) || empty($viaje)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $this->viajeroRepository->saveViajero($cedula, $nombre, $fecha, $telefono,$viaje);

        return new JsonResponse(['status' => 'viajero creado!'], Response::HTTP_CREATED);
    }

    /**
     * @Route("viajero/{id}", name="get_one_viajero", methods={"GET"})
     */
    public function get($id): JsonResponse
    {
        $viajero = $this->viajeroRepository->findOneBy(['id' => $id]);

        $data = [
            'id' => $viajero->getId(),
            'nombre' => $viajero->getNombre(),
            'cedula' => $viajero->getCedula(),
            'fecha' => $viajero->getFecha(),
            'telefono' => $viajero->getTelefono(),
            'viaje' => $viajero->getViaje(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("viajeros", name="get_all_viajeros", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        $viajeros = $this->viajeroRepository->findAll();
        $data = [];

        foreach ($viajeros as $viajero) {
            $data[] = [
                'id' => $viajero->getId(),
                'nombre' => $viajero->getNombre(),
                'cedula' => $viajero->getCedula(),
                'fecha' => $viajero->getFecha(),
                'telefono' => $viajero->getTelefono(),
                'viaje' => $viajero->getViaje(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("viajero/{id}", name="update_viajero", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $viajero = $this->viajeroRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);

        empty($data['nombre']) ? true : $viajero->setNombre($data['nombre']);
        empty($data['cedula']) ? true : $viajero->setCedula($data['cedula']);
        empty($data['fecha']) ? true : $viajero->setFecha($data['fecha']);
        empty($data['telefono']) ? true : $viajero->setTelefono($data['telefono']);
        empty($data['viaje']) ? true : $viajero->setViaje($data['viaje']);

        $updatedViajero = $this->viajeroRepository->updateViajero($viajero);

		return new JsonResponse(['status' => 'Viajero actualizado!'], Response::HTTP_OK);
    }

    /**
     * @Route("viajero/{id}", name="delete_viajero", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        $viajero = $this->viajeroRepository->findOneBy(['id' => $id]);

        $this->viajeroRepository->removeViajero($viajero);

        return new JsonResponse(['status' => 'Viajero deleted'], Response::HTTP_OK);
    }


}
