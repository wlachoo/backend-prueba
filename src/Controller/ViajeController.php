<?php

namespace App\Controller;
use App\Repository\ViajeRepository;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ViajeController
{
    private $viajeRepository;
 
    public function __construct(ViajeRepository $viajeRepository)
    {
        $this->viajeRepository = $viajeRepository;
    }
    /**
     * @Route("viaje", name="add_viaje", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $codigo = $data['codigo'];
        $numero_plazas = $data['numero_plazas'];
        $destino = $data['destino'];
        $lugar = $data['lugar']; 
        $precio = $data['precio'];

        if (empty($codigo) || empty($numero_plazas) || empty($destino) || empty($lugar) || empty($precio)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        } 

        $this->viajeRepository->saveViaje($codigo, $numero_plazas, $destino, $lugar, $precio);

        return new JsonResponse(['status' => 'viaje created!'], Response::HTTP_CREATED);
    }
    /**
     * @Route("viaje/{id}", name="get_one_viaje", methods={"GET"})
     */
    public function get($id): JsonResponse
    {
        $viaje = $this->viajeRepository->findOneBy(['id' => $id]);

        $data = [
            'id' => $viaje->getId(),
            'codigo' => $viaje->getCodigo(),
            'numero_plazas' => $viaje->getNumeroPlazas(),
            'destino' => $viaje->getDestino(),
            'lugar' => $viaje->getLugar(),
            'precio' => $viaje->getPrecio(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }
    /**
     * @Route("viajes", name="get_all_viajes", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        $viajes = $this->viajeRepository->findAll();
        $data = [];

        foreach ($viajes as $viaje) {
            $data[] = [
                'id' => $viaje->getId(),
                'codigo' => $viaje->getCodigo(),
                'numero_plazas' => $viaje->getNumeroPlazas(),
                'destino' => $viaje->getDestino(),
                'lugar' => $viaje->getLugar(),
                'precio' => $viaje->getPrecio(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }
    /**
     * @Route("viaje/{id}", name="update_viaje", methods={"PUT"})
     */
    public function updateViaje($id, Request $request): JsonResponse
    {
        $viaje = $this->viajeRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);

        empty($data['codigo']) ? true : $viaje->setCodigo($data['codigo']);
        empty($data['numero_plazas']) ? true : $viaje->setNumeroPlazas($data['numero_plazas']);
        empty($data['destino']) ? true : $viaje->setDestino($data['destino']);
        empty($data['lugar']) ? true : $viaje->setLugar($data['lugar']);
        empty($data['precio']) ? true : $viaje->setPrecio($data['precio']);

        $updatedviaje = $this->viajeRepository->updateviaje($viaje);

		return new JsonResponse(['status' => 'Viajero actualizado!'], Response::HTTP_OK);
    }

    /**
     * @Route("viaje/{id}", name="delete_viaje", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        $viaje = $this->viajeRepository->findOneBy(['id' => $id]);

        $this->viajeRepository->removeviaje($viaje);

        return new JsonResponse(['status' => 'Viaje deleted'], Response::HTTP_OK);
    }
}
