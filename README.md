<<<<<<< README.md
# Uso e instalación

## 1. `composer update` y  2. `composer install`

instalar las dependencias

## 3. DATABASE_URL="postgresql://<db_user>:<db_password>@127.0.0.1:5432/<db_name>?serverVersion=13&charset=utf8" 
 
modificar archivo .env con los datos de conexion

## 4. `bin/console make:migration` y 5. `bin/console doctrine:migrations:migrate`

correr las migraciones

## 6. `symfony server:start`

Ejecuta la aplicación en el modo de desarrollo.\
Abra [http://127.0.0.1:8000](http://localhost:8000) para verlo en su navegador.

